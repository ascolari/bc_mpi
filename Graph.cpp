#include "Graph.hpp"

int Graph::parse_from_file( char *file )
{
    std::string str_file(file);

    if(str_file.compare(str_file.size()-4, 4, ".bit") == 0) {
        return this->parse_bitfile(file);
    } else {
        return this->parse_edgelist(file);
    }
}

int Graph::parse_edgelist ( char *file )
{
    std::set<std::string> vertices;

    //Scan the file
    std::ifstream edgelist ( file,std::ifstream::in );
    std::string line;

    if ( !edgelist.good() ) {
        return -1;
    }

    std::vector<std::string> from;
    std::vector<std::string> to;
    while ( std::getline ( edgelist,line ) ) {
        if ( ( line[0] == '%' ) || ( line[0] == '#' ) ) { //Allow comments
            continue;
        }

        std::vector<std::string> splitvec;
        boost::split ( splitvec,line,boost::is_any_of ( " \t" ),boost::token_compress_on );

        for ( unsigned i=0; i< 2 && i<splitvec.size(); i++ ) {
            vertices.insert ( splitvec[i] );
        }

        from.push_back ( splitvec[0] );
        to.push_back ( splitvec[1] );
    }

    edgelist.close();

    this->num_vertex = vertices.size();
    this->num_edge = from.size();

    unsigned id = 0;
    for ( std::set<std::string>::iterator i = vertices.begin(), e = vertices.end(); i!=e; ++i ) {
        this->IDs.insert ( boost::bimap<unsigned,std::string>::value_type ( id++,*i ) );
    }



    std::vector< std::pair<unsigned,unsigned> > edges;
    unsigned j = 0;
    for ( unsigned i=0; i<this->num_edge; i++ ) {
        boost::bimap<unsigned,std::string>::right_map::iterator itf = this->IDs.right.find ( from[i] );
        boost::bimap<unsigned,std::string>::right_map::iterator itc = this->IDs.right.find ( to[i] );

        if ( ( itf == this->IDs.right.end() ) || ( itc == this->IDs.right.end() ) ) {
            std::cerr << "Error parsing graph file." << std::endl;
            exit ( -1 );
        } else {
            if ( itf->second == itc->second ) {
                // auto-edge: just skip
                continue;
            }
            //Treat undirected edges as two directed edges       
            edges.push_back ( std::make_pair (itf->second, itc->second) );
            edges.push_back ( std::make_pair (itc->second, itf->second) );
            j+=2;
        }
    }
    this->num_edge = j;

    //Sort edges by F and C
    std::sort ( edges.begin(),edges.end(), [](std::pair<unsigned,unsigned> a, std::pair<unsigned,unsigned> b) {
            return a.first < b.first || (a.first == b.first && a.second < b.second);
    });
    //By default, pair sorts with precedence to it's first member, which is precisely what we want.
    this->R = new unsigned int[this->num_vertex+1];
    this->F = new unsigned int[this->num_edge];
    this->C = new unsigned int[this->num_edge];

    this->R[0] = 0;
    unsigned last_node = 0;
    unsigned lastf = UINT_MAX;
    unsigned lastc = UINT_MAX;
    j = 0;
    for ( unsigned i=0; i < this->num_edge; i++ ) {
        if (lastf != edges[i].first || lastc != edges[i].second) {
            this->F[j] = lastf = edges[i].first;
            this->C[j] = lastc = edges[i].second;
            while ( edges[i].first > last_node ) {
                this->R[++last_node] = j;
            }
            j++;
        }
    }
    this->R[this->num_vertex] = j;
    this->num_edge = j;
    edges.clear();

    return 0;
}

int Graph::parse_bitfile( char *file )
{
    //Scan the file
    std::ifstream in ( file, std::ifstream::binary );
    char * data;

    if ( !in.good() ) {
        return -1;
    }

    long start_pos = in.tellg();
    in.ignore(std::numeric_limits<std::streamsize>::max());
    long char_count = in.gcount();
    in.seekg(start_pos);

    data = new char[char_count];
    in.read(data, char_count);
    this->deserialize(data);

    in.close();
    delete data;

    return 0;
}

int Graph::write_to_bitfile( char *file )
{
    std::ofstream out ( file, std::ofstream::binary );

    if ( !out.good() ) {
        return -1;
    }

    size_t size = this->serialize_size();
    char* data = new char[size];

    this->serialize(data);
    out.write(data, size);
    out.close();
    delete data;

    return 0;
}

size_t Graph::serialize_size()
{
    return sizeof(this->num_vertex) + sizeof(this->num_edge) + 
        sizeof(unsigned int)*(this->num_edge + this->num_vertex+1);
}

void Graph::serialize( char * dataOut )
{
    size_t size;

    // this->num_vertex
    size = sizeof(unsigned);
    *((unsigned*)dataOut) = this->num_vertex;
    dataOut += size;

    // this->num_edge
    size = sizeof(unsigned);
    *((unsigned*)dataOut) = this->num_edge;
    dataOut += size;

    // this->R
    size = sizeof(unsigned int)*(this->num_vertex + 1);
    memcpy((void *)dataOut, (void *)this->R, size);
    dataOut += size;

    // this->C
    size = sizeof(unsigned int)*(this->num_edge);
    memcpy((void *)dataOut, (void *)this->C, size);
    dataOut += size;
}

void Graph::deserialize( char * dataIn )
{
    size_t size;

    // this->num_vertex
    size = sizeof(unsigned);
    this->num_vertex = *((unsigned*)dataIn);
    dataIn += size;

    // this->num_edge
    size = sizeof(unsigned);
    this->num_edge = *((unsigned*)dataIn);
    dataIn += size;

    // this->R
    this->R = new unsigned int[this->num_vertex+1];
    size = sizeof(unsigned int)*(this->num_vertex + 1);
    memcpy((void *)this->R, (void *)dataIn, size);
    dataIn += size;

    // this->C
    this->C = new unsigned int[this->num_edge];
    size = sizeof(unsigned int)*(this->num_edge);
    memcpy((void *)this->C, (void *)dataIn, size);
    dataIn += size;

    // Generate C array
    this->F = new unsigned int[this->num_edge];
    unsigned j = 0;
    for( unsigned i = 0; i < this->num_vertex; i++ ) {
        for(; j < this->R[i+1]; j++ ) {
            this->F[j] = i;
        }
    }
}

void Graph::print_R()
{
    if ( R == NULL ) {
        std::cerr << "Error: Attempt to prunsignedCSR of a graph that has not been parsed." << std::endl;
    }

    std::cout << "R = [";
    for ( unsigned i=0; i< ( num_vertex+1 ); i++ ) {
        if ( i == this->num_vertex ) {
            std::cout << R[i] << "]" << std::endl;
        } else {
            std::cout << R[i] << ",";
        }
    }
}

void Graph::print_number_of_isolated_vertices()
{
    if ( R == NULL ) {
        std::cerr << "Error: Attempt to prunsignedCSR of a graph that has not been parsed." << std::endl;
    }

    unsigned isolated = 0;
    for ( unsigned i=0; i<num_vertex; i++ ) {
        unsigned degree = R[i+1]-R[i];
        if ( degree == 0 ) {
            isolated++;
        }
    }

    std::cout << "Number of isolated vertices: " << isolated << std::endl;
}

void Graph::print_CSR()
{
    if ( ( R == NULL ) || ( C == NULL ) || ( F == NULL ) ) {
        std::cerr << "Error: Attempt to prunsignedCSR of a graph that has not been parsed." << std::endl;
        exit ( -1 );
    }

    std::cout << "R = [";
    for ( unsigned i=0; i< ( num_vertex+1 ); i++ ) {
        if ( i == num_vertex ) {
            std::cout << R[i] << "]" << std::endl;
        } else {
            std::cout << R[i] << ",";
        }
    }

    std::cout << "C = [";
    for ( unsigned i=0; i< ( 2*num_edge ); i++ ) {
        if ( i == ( ( 2*num_edge )-1 ) ) {
            std::cout << C[i] << "]" << std::endl;
        } else {
            std::cout << C[i] << ",";
        }
    }

    std::cout << "F = [";
    for ( unsigned i=0; i< ( 2*num_edge ); i++ ) {
        if ( i == ( ( 2*num_edge )-1 ) ) {
            std::cout << F[i] << "]" << std::endl;
        } else {
            std::cout << F[i] << ",";
        }
    }
}

void Graph::print_high_degree_vertices()
{
    if ( R == NULL ) {
        std::cerr << "Error: Attempt to search adjacency list of graph that has not been parsed." << std::endl;
        exit ( -1 );
    }

    unsigned max_degree = 0;
    for ( unsigned i=0; i<num_vertex; i++ ) {
        unsigned degree = R[i+1]-R[i];
        if ( degree > max_degree ) {
            max_degree = degree;
            std::cout << "Max degree: " << degree << std::endl;
        }
    }
}

void Graph::print_adjacency_list()
{
    if ( R == NULL ) {
        std::cerr << "Error: Attempt to prunsignedadjacency list of graph that has not been parsed." << std::endl;
        exit ( -1 );
    }

    std::cout << "Edge lists for each vertex: " << std::endl;

    for ( unsigned i=0; i<num_vertex; i++ ) {
        unsigned begin = R[i];
        unsigned end = R[i+1];
        boost::bimap<unsigned,std::string>::left_map::iterator itr = IDs.left.find ( i );
        for ( unsigned j=begin; j<end; j++ ) {
            boost::bimap<unsigned,std::string>::left_map::iterator itc = IDs.left.find ( C[j] );
            if ( j==begin ) {
                std::cout << itr->second << " | " << itc->second;
            } else {
                std::cout << ", " << itc->second;
            }
        }
        if ( begin == end ) { //Single, unconnected node
            std::cout << itr->second << " | ";
        }
        std::cout << std::endl;
    }
}

void Graph::print_numerical_edge_file ( char *outfile )
{
    std::ofstream ofs ( outfile, std::ios::out );
    if ( !ofs.good() ) {
        std::cerr << "Error opening output file." << std::endl;
        exit ( -1 );
    }
    for ( unsigned i=0; i<2*num_edge; i++ ) {
        if ( F[i] < C[i] ) {
            ofs << F[i] << " " << C[i] << std::endl;
        }
    }
}

void Graph::print_BC_scores ( const std::vector<float> &bc, char *outfile ) {
    std::vector<double> bc_double(bc.begin(), bc.end());
    print_BC_scores(bc_double, outfile);
}

void Graph::print_BC_scores ( const std::vector<double> &bc, char *outfile )
{
    std::ofstream ofs;
    if ( outfile != NULL ) {
        ofs.open ( outfile, std::ios::out );
    }
    std::ostream &os = ( outfile ? ofs : std::cout );
    for ( unsigned i=0; i<num_vertex; i++ ) {
        boost::bimap<unsigned,std::string>::left_map::iterator it = IDs.left.find ( i );
        if ( it != IDs.left.end() ) {
            os << it->second << " " << bc[i] << std::endl;
        } else {
            //Generate vertex name from its id
            os << (i+1) << " " << bc[i] << std::endl;
        }
    }
}
