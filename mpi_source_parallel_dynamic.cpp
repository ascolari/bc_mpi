#include "mpi.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <fstream>
#include <algorithm>
#include <iostream>
#include "Graph.hpp"
#include <vector>
#include <queue>
#include <stack>
#include <set>
#include <atomic>

#include <sys/time.h>

struct config_t {
    int init_error;
    unsigned problem_size;
    unsigned source_limit;   // limit on the computation for testing
    unsigned schedule_block;
};

struct events_t {
    struct timeval begin;   // MPI init
    struct timeval init;    // get problem data form disk
    struct timeval broadcast_config;
    struct timeval serialize;
    struct timeval broadcast_problem;
    struct timeval end_broadcast;
    struct timeval reduce;
    struct timeval finalize;
    struct timeval end;
};

struct worker_events_t {
    struct timeval deserialize;
    struct timeval compute;
    struct timeval end_compute;
    unsigned long compute_time;
    unsigned long waiting_time;
};

struct message_t {
    unsigned long int start_vertex;
    unsigned long int end_vertex;
};

#define BC_TYPE MPI_DOUBLE
typedef double bc_type;

void generateReport(struct events_t &events, struct worker_events_t* worker_events, 
    int workers, int iterations, Graph &g, char *report_filename);

unsigned long timevalToLong(struct timeval tv);
unsigned long timevalDiff(struct timeval tv_end, struct timeval tv_begin);


int main ( int argc, char *argv[] )
{
    struct events_t events;
    struct worker_events_t worker_events;
    struct message_t message;
    struct timeval t1, t2;
    int rank, numprocs;

    worker_events.compute_time = 0;
    worker_events.waiting_time = 0;

    // #EVENT [begin]
    gettimeofday ( &events.begin, NULL );

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

    // Problem data
    config_t config;
    Graph g;

    // Master process variables
    bool printBC = false;
    char* output_filename = NULL;
    char* report_filename = NULL;
    
    if(rank == 0)
    {
        // #EVENT [init]
        gettimeofday ( &events.init, NULL );

        if(numprocs < 2) {
            std::cerr << "At least 2 MPI processes are required." << std::endl;
            exit(-1);
        } 

        // Master process initialization
        if ( argc < 2 ) {

            std::cout << "Command usage: " << argv[0] << " [options] ./GRAPH_PATH" << std::endl;
            config.init_error = 1;

        } else {

            int c;
            extern char* optarg;

            // Set default config
            config.init_error = 0;
            config.source_limit = UINT_MAX;
            config.schedule_block = 1;

            while ((c = getopt (argc, argv, "s:po:b:r:")) != -1) {
                switch (c) {
                    case 's':
                        errno = 0;
                        config.source_limit = (unsigned) strtoul(optarg, NULL, 10);
                        if (errno) {
                            std::cout << "max source is in wrong format" << std::endl;
                            config.init_error = 1;
                        }
                        break;
                    case 'p':
                        printBC = true;
                        break;
                    case 'o':
                        printBC = true;
                        output_filename = optarg;
                        break;
                    case 'r':
                        report_filename = optarg;
                        break;
                    case 'b':
                        errno = 0;
                        config.schedule_block = (unsigned) strtoul(optarg, NULL, 10);
                        if (errno) {
                            std::cout << "schedule block is in wrong format" << std::endl;
                            config.init_error = 1;
                        }
                        break;
                }
            }

            int read_result = g.parse_from_file(argv[argc-1]);

            if(read_result != 0) {
                config.init_error = -1;
                std::cerr << "Error opening graph file." << std::endl;
            } else {
                config.problem_size = g.serialize_size();
            }
        }
    }

    if(rank == 0) {
        // #EVENT [broadcast_config]
        gettimeofday ( &events.broadcast_config, NULL );
    }

    // Broadcast problem configuration
    MPI_Bcast(&config, sizeof(config), MPI_BYTE, 0, MPI_COMM_WORLD);


    // Check for errors
    if (config.init_error != 0) {
        MPI_Finalize();
        if(config.init_error < 0) {
            exit(config.init_error);
        } else {
            exit(0);
        }
    }

    if(rank == 0) {
        // #EVENT [serialize]
        gettimeofday ( &events.serialize, NULL );
    }

    // Allocate memory for the graph
    char *problem_data = new char[config.problem_size];
    if(rank == 0) {
        g.serialize(problem_data);
    }

    if(rank == 0) {
        // #EVENT [broadcast_problem]
        gettimeofday ( &events.broadcast_problem, NULL );
    }

    // Distribute the graph to all the processes
    MPI_Bcast(problem_data, config.problem_size, MPI_BYTE, 0, MPI_COMM_WORLD);

    if(rank == 0) {
        // #EVENT [end_broadcast]
        gettimeofday ( &events.end_broadcast, NULL );
    }

    // #EVENT [deserialize] DISTRIBUTED
    gettimeofday ( &worker_events.deserialize, NULL );
    
    // Deserialize data on slave processes
    if(rank != 0) {
        g.deserialize(problem_data);
    }
    delete problem_data;
    
    // Start multithreaded computation on each node
    unsigned iterations = g.num_vertex > config.source_limit ? config.source_limit : g.num_vertex;

    // #EVENT [compute] DISTRIBUTED
    gettimeofday ( &worker_events.compute, NULL );


    std::vector<unsigned> Q_curr ( g.num_vertex,0 );
    std::vector<unsigned> Q_next ( g.num_vertex,0 );
    std::vector<unsigned> S ( g.num_vertex,0 );
    std::vector<unsigned> ends ( g.num_vertex + 1,0 );
    std::vector<unsigned> d ( g.num_vertex, UINT_MAX );
    std::vector<unsigned> sigma ( g.num_vertex,0 );
    std::vector<bc_type> delta ( g.num_vertex,0 );

    bc_type* bc = new bc_type[g.num_vertex];
    memset((void*) bc, 0, sizeof(bc_type) * g.num_vertex);

    //char* mpi_message_buffer = NULL;
    //int mpi_buffer_size;
    message_t *worker_messages = NULL;
    MPI_Status mpi_status;
    MPI_Request request;

    
    if(rank == 0) 
    {
        // MASTER NODE (scheduler)

        // Initialize buffer for worker messages
        //mpi_buffer_size = (sizeof(message_t) + MPI_BSEND_OVERHEAD) * numprocs * 8;
        //mpi_message_buffer = new char[mpi_buffer_size];
        worker_messages = new message_t[numprocs];

        //MPI_Buffer_attach(mpi_message_buffer, mpi_buffer_size);
        
        for( unsigned source = 0; source < iterations; ) {
            
            // Wait for a ready worker
            MPI_Recv(NULL, 0, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);
            
            // Compute unit of work
            int worker = mpi_status.MPI_SOURCE;
            worker_messages[worker].start_vertex = source;

            source = source + config.schedule_block;
            if(source >= iterations) {
                source = iterations;
            }

            worker_messages[worker].end_vertex = source;
            
            // Assign work
            MPI_Send(&worker_messages[worker], sizeof(message_t), MPI_BYTE, worker, 0, MPI_COMM_WORLD);
        }

        // Work assignment completed, inform all workers
        message.start_vertex = iterations;
        message.end_vertex = iterations;

        for( int worker = 0; worker < numprocs - 1; worker++ ) {
            MPI_Recv(NULL, 0, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);
            //MPI_Ibsend(&message, sizeof(message_t), MPI_BYTE, mpi_status.MPI_SOURCE, 0, MPI_COMM_WORLD, &request);
            MPI_Send(&message, sizeof(message_t), MPI_BYTE, mpi_status.MPI_SOURCE, 0, MPI_COMM_WORLD);
        }
    }
    else
    {
        bool done = false;

        while(!done) {
            
            gettimeofday ( &t1, NULL );

            // SEND ready command
            MPI_Isend(NULL, 0, MPI_BYTE, 0, 0, MPI_COMM_WORLD, &request);

            // WAIT for action
            MPI_Recv(&message, sizeof(message_t), MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);

            gettimeofday ( &t2, NULL );

            worker_events.waiting_time += timevalDiff(t2,t1);
            
            done = message.end_vertex == message.start_vertex;

            // WORKER NODE (compute)
            for ( unsigned source = message.start_vertex; source < message.end_vertex; source ++ ) {

                int depth = 0;
                unsigned Q_curr_len = 0;
                unsigned Q_next_len = 0;
                unsigned S_len = 0;
                unsigned ends_len = 0;

                //Initialization
                for ( unsigned v = 0; v < g.num_vertex; v++ ) {
                    if ( v == source ) {
                        d[v] = 0;
                        sigma[v] = 1;
                    } else {
                        d[v] = UINT_MAX;
                        sigma[v] = 0;
                    }
                    delta[v] = 0;
                }
                Q_curr[0] = source;
                Q_curr_len = 1;
                Q_next_len = 0;
                S[0] = source;
                S_len = 1;
                ends[0] = 1;
                ends[1] = 1;
                ends_len = 2;
                depth = 0;


                //Work efficient shortest path calculation
                while ( true ) {

                    for ( unsigned tid = 0; tid < Q_curr_len; tid++ ) {
                        unsigned v = Q_curr[tid];
                        
                        for ( unsigned j=g.R[v]; j<g.R[v+1]; j++ ) { //for each neighbor of v
                            unsigned w = g.C[j];

                            //intel atomic compare and swap
                            if ( __sync_bool_compare_and_swap ( &d[w], UINT_MAX, ( d[v]+1 ) ) ) {
                                unsigned int temp = __sync_fetch_and_add ( &Q_next_len, 1 );
                                Q_next[temp] = w;

                            }
                            if ( d[w] == ( d[v]+1 ) ) {
                                __sync_fetch_and_add ( & ( sigma[w] ), sigma[v] );
                            }
                        }
                    }

                    if ( Q_next_len == 0 ) {
                        depth = d[S[S_len-1]];
                        break;
                    } else {
                        for ( unsigned tid = 0; tid < Q_next_len; tid++ ) {
                            Q_curr[tid] = Q_next[tid];
                            S[tid + S_len] = Q_next[tid];
                        }
                        ends[ends_len] = ends[ends_len-1] + Q_next_len;
                        ends_len++;
                        Q_curr_len = Q_next_len;
                        S_len = S_len + Q_next_len;
                        Q_next_len = 0;
                    }
                }

                // Dependency accumulation
                // Skip last iteration (delta values are 0!)
                depth--;
                while ( depth > 0 ) {
                    
                    for ( unsigned tid = ends[depth]; tid < ends[depth+1]; tid++ ) {
                        unsigned w = S[tid];
                        bc_type dsw = 0.0f;
                        unsigned sw = sigma.at ( w );

                        for ( unsigned j=g.R[w]; j<g.R[w+1]; j++ ) { //for each neighbor of v
                            unsigned v = g.C[j];
                            if ( d[v] == ( d[w]+1 ) ) {
                                dsw += ( sw/ ( bc_type ) sigma[v] ) * ( 1+delta[v] );
                            }
                        }

                        delta[w] = dsw;
                    }

                    depth--;
                }
                for ( unsigned i = 0; i < g.num_vertex; i++ ) {
                    bc[i] += delta[i];
                }
            }

            gettimeofday ( &t1, NULL );

            worker_events.compute_time += timevalDiff(t1,t2);
        }       
    }

    // #EVENT [end_compute] DISTRIBUTED
    gettimeofday ( &worker_events.end_compute, NULL );
    

    // Reduce data from all the processes
    if(rank == 0) {
        // #EVENT [reduce]
        gettimeofday ( &events.reduce, NULL );
        MPI_Reduce(MPI_IN_PLACE, bc, g.num_vertex, BC_TYPE, MPI_SUM, 0, MPI_COMM_WORLD);
    }
    else {
        MPI_Reduce(bc, bc, g.num_vertex, BC_TYPE, MPI_SUM, 0, MPI_COMM_WORLD);
    }


    if(rank == 0) {
        // #EVENT [finalize]
        gettimeofday ( &events.finalize, NULL );

        //MPI_Buffer_detach(mpi_message_buffer, &mpi_buffer_size);
        //delete mpi_message_buffer;
        delete worker_messages;

        for ( unsigned i=0; i < g.num_vertex; i++ ) {
            // Undirected edges are modeled as two directed edges, but the scores shouldn't be double counted.
            bc[i] /= 2.0f;
        }

        if(printBC) {
            std::vector<bc_type> bc_vec (bc, bc + g.num_vertex);
            g.print_BC_scores ( bc_vec, output_filename );    
        }

    }

    delete bc;

    if(rank == 0) {
        // #EVENT [end]
        gettimeofday ( &events.end, NULL );
    }

    //Get time data from the other processors and report events
    if(rank == 0) {
        struct worker_events_t *all_worker_events = new worker_events_t[numprocs];
        MPI_Gather(&worker_events, sizeof(worker_events_t), MPI_BYTE, all_worker_events, sizeof(worker_events_t), MPI_BYTE, 0, MPI_COMM_WORLD);
        generateReport(events, all_worker_events, numprocs, iterations, g, report_filename);
        delete all_worker_events;
    } else {
        MPI_Gather(&worker_events, sizeof(worker_events_t), MPI_BYTE, NULL, sizeof(worker_events_t), MPI_BYTE, 0, MPI_COMM_WORLD );
    }
    
    MPI_Finalize();

    return 0;
}



/* ----- reports methods ----- */

unsigned long timevalDiff(struct timeval tv_end, struct timeval tv_begin)
{
    return timevalToLong(tv_end) - timevalToLong(tv_begin);
}

unsigned long timevalToLong(struct timeval tv)
{
    return (unsigned long)tv.tv_sec * 1000000L + tv.tv_usec;
}

void generateReport(struct events_t &events, struct worker_events_t* worker_events, 
    int workers, int iterations, Graph &g, char *report_filename)
{
    std::ofstream ofs;
    if ( report_filename != NULL ) {
        ofs.open ( report_filename, std::ios::out );
    }
    std::ostream &os = ( report_filename ? ofs : std::cout );

    os << "- PERFORMANCE REPORT -" << std::endl;
    os << std::endl;

    os << "** PROBLEM" << std::endl;
    os << "vertices=" << g.num_vertex << std::endl;
    os << "edges=" << g.num_edge << std::endl;
    os << "processors=" << workers << std::endl;
    os << "iterations=" << iterations << std::endl;
    os << std::endl;

    os << "** MASTER EVENTS" << std::endl;
    os << "begin=" << timevalToLong(events.begin) << std::endl;
    os << "init=" << timevalToLong(events.init) << std::endl;
    os << "broadcast_config=" << timevalToLong(events.broadcast_config) << std::endl;
    os << "serialize=" << timevalToLong(events.serialize) << std::endl;
    os << "broadcast_problem=" << timevalToLong(events.broadcast_problem) << std::endl;
    os << "end_broadcast=" << timevalToLong(events.end_broadcast) << std::endl;
    os << "reduce=" << timevalToLong(events.reduce) << std::endl;
    os << "finalize=" << timevalToLong(events.finalize) << std::endl;
    os << "end=" << timevalToLong(events.end) << std::endl;
    os << std::endl;

    os << "** WORKERS EVENTS" << std::endl;
    for(int i = 1; i < workers; i++) {
        os << "WORKER " << i << std::endl;
        os << "deserialize=" << timevalToLong(worker_events[i].deserialize) << std::endl;
        os << "compute=" << timevalToLong(worker_events[i].compute) << std::endl;
        os << "end_compute=" << timevalToLong(worker_events[i].end_compute) << std::endl;
        os << "compute_time=" << worker_events[i].compute_time << std::endl;
        os << "waiting_time=" << worker_events[i].waiting_time << std::endl;
    }
    os << std::endl;

    os << "** METRICS" << std::endl;

    unsigned long int mpi_init_time = 
        timevalDiff(events.init, events.begin); // MPI init

    unsigned long int mpi_communication_time = 
        timevalDiff(events.serialize, events.broadcast_config) + // coniguration broadcast
        timevalDiff(events.end_broadcast, events.broadcast_problem) + // problem broadcast
        timevalDiff(events.finalize, events.reduce); // final reduce
        
    unsigned long int serialization_time = 
        timevalDiff(events.broadcast_problem, events.serialize);  // master serialization

    unsigned long int compute_time = 
        timevalDiff(events.reduce, events.end_broadcast);

    unsigned long int io_time =
        timevalDiff(events.serialize, events.init) + // read data from disk
        timevalDiff(events.end, events.finalize); // write data to disk

    unsigned long int overall_time = timevalDiff(events.end, events.begin);

    os << "+ MASTER BREAKDOWN" << std::endl;
    os << "mpi_init_time=" << mpi_init_time << std::endl;
    os << "mpi_communication_time=" << mpi_communication_time << std::endl;
    os << "serialization_time=" << serialization_time << std::endl;
    os << "compute_time=" << compute_time << std::endl;
    os << "io_time=" << io_time << std::endl;
    os << std::endl;
    os << "overall_time=" << overall_time << std::endl;
    os << std::endl;
    

    os << "+ WORKERS BREAKDOWN" << std::endl;

    unsigned long int avg_deserialization_time, min_deserialization_time, max_deserialization_time;
    avg_deserialization_time = min_deserialization_time = max_deserialization_time = timevalDiff(worker_events[1].compute, worker_events[1].deserialize);

    for(int i = 2; i < workers; i++) {
        unsigned long int new_val = timevalDiff(worker_events[i].compute, worker_events[i].deserialize);
        avg_deserialization_time += new_val;
        min_deserialization_time = new_val < min_deserialization_time ? new_val : min_deserialization_time;
        max_deserialization_time = new_val > max_deserialization_time ? new_val : max_deserialization_time;
    }
    avg_deserialization_time /= (workers - 1);

    os << "avg_deserialization_time=" << avg_deserialization_time << std::endl;
    os << "max_deserialization_time=" << max_deserialization_time << std::endl;
    os << "min_deserialization_time=" << min_deserialization_time << std::endl;


    unsigned long int avg_compute_time, min_compute_time, max_compute_time;
    avg_compute_time = min_compute_time = max_compute_time = timevalDiff(worker_events[1].end_compute, worker_events[1].compute);

    for(int i = 2; i < workers; i++) {
        unsigned long int new_val = timevalDiff(worker_events[i].end_compute, worker_events[i].compute);
        avg_compute_time += new_val;
        min_compute_time = new_val < min_compute_time ? new_val : min_compute_time;
        max_compute_time = new_val > max_compute_time ? new_val : max_compute_time;
    }
    avg_compute_time /= (workers - 1);

    os << "avg_compute_time=" << avg_compute_time << std::endl;
    os << "max_compute_time=" << max_compute_time << std::endl;
    os << "min_compute_time=" << min_compute_time << std::endl;

    os << std::endl;

    unsigned long int avg_worker_time, min_worker_time, max_worker_time;
    avg_worker_time = min_worker_time = max_worker_time = timevalDiff(worker_events[1].end_compute, worker_events[1].deserialize);

    for(int i = 2; i < workers; i++) {
        unsigned long int new_val = timevalDiff(worker_events[i].end_compute, worker_events[i].deserialize);
        avg_worker_time += new_val;
        min_worker_time = new_val < min_worker_time ? new_val : min_worker_time;
        max_worker_time = new_val > max_worker_time ? new_val : max_worker_time;
    }
    avg_worker_time /= (workers - 1);

    os << "avg_worker_time=" << avg_worker_time << std::endl;
    os << "max_worker_time=" << max_worker_time << std::endl;
    os << "min_worker_time=" << min_worker_time << std::endl;

    os << std::endl;

    
    unsigned long int lostTime = max_worker_time - avg_worker_time;

    os << "Lost time due to unbalance: " << lostTime <<  " / " << overall_time << " (" << ((float)lostTime*100 / overall_time ) << " %)" << std::endl;

    double elapsed = ( ( double ) ( ( events.end.tv_sec * 1000000 + events.end.tv_usec ) - ( events.begin.tv_sec * 1000000 + events.begin.tv_usec ) ) ) /1000000;
    std::cout << "time " << elapsed << std::endl;
}

