# BC_MPI

Project for the PhD class of openMP and MPI programming, taught at Politecnico di Milano in 2015/2016.
The implementation is in C++ 2011.

For the literature, read http://dl.acm.org/citation.cfm?id=2683656
