#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <fstream>
#include <algorithm>
#include <iostream>
#include "Graph.hpp"
#include <vector>
#include <queue>
#include <stack>
#include <set>
#include <atomic>

#include <sys/time.h>

int main ( int argc, char *argv[] )
{
    struct timeval start, end;
    unsigned source_limit = UINT_MAX;
    bool printBC = false;
    char* output_filename = NULL;

    if ( argc < 2 ) {
        std::cout << "Command usage: " << argv[0] << " [options] ./GRAPH_PATH" << std::endl;
        return 0;
    }
    
    int c;
    extern char* optarg;
    while ((c = getopt (argc, argv, "s:po:")) != -1) {
        switch (c) {
        case 's':
            errno = 0;
            source_limit = (unsigned) strtoul(optarg, NULL, 10);
            if (errno) {
                std::cout << "max source is in wrong format" << std::endl;
                exit(-1);
            }
            break;
        case 'p':
            printBC = true;
            break;
        case 'o':
            printBC = true;
            output_filename = optarg;
            break;
        }
    }
    
    Graph g;
    if(g.parse_from_file ( argv[argc-1] ) != 0) {
        std::cerr << "Error opening graph file." << std::endl;
        exit ( -1 );
    }

    std::vector<float> bc ( g.num_vertex,0 );


    gettimeofday ( &start, NULL );
    unsigned iterations = g.num_vertex > source_limit ? source_limit : g.num_vertex;

    #ifdef PRINT_UNBALANCE_INFO
        std::vector<double> iter_time ( g.num_vertex, 0 );
        std::vector<double> thread_time ( omp_get_max_threads(), 0 );
        std::vector<double> source_time ( iterations, 0 );
    #endif

    #pragma omp parallel
    {
        std::vector<unsigned> Q_curr ( g.num_vertex,0 );
        std::vector<unsigned> Q_next ( g.num_vertex,0 );
        std::vector<unsigned> S ( g.num_vertex,0 );
        std::vector<unsigned> ends ( g.num_vertex + 1,0 );
        std::vector<unsigned> d ( g.num_vertex, UINT_MAX );
        std::vector<unsigned> sigma ( g.num_vertex,0 );
        std::vector<float> delta ( g.num_vertex,0 );

        #ifdef PRINT_UNBALANCE_INFO
            struct timeval iter_start, iter_end;
        #endif

        #pragma omp for
        for ( unsigned source = 0; source < iterations; source++ ) {

            #ifdef PRINT_UNBALANCE_INFO
                gettimeofday ( &iter_start, NULL );
            #endif

            int depth = 0;
            unsigned Q_curr_len = 0;
            unsigned Q_next_len = 0;
            unsigned S_len = 0;
            unsigned ends_len = 0;

            //Initialization
            for ( unsigned v = 0; v < g.num_vertex; v++ ) {
                if ( v == source ) {
                    d[v] = 0;
                    sigma[v] = 1;
                } else {
                    d[v] = UINT_MAX;
                    sigma[v] = 0;
                }
                delta[v] = 0;
            }
            Q_curr[0] = source;
            Q_curr_len = 1;
            Q_next_len = 0;
            S[0] = source;
            S_len = 1;
            ends[0] = 1;
            ends[1] = 1;
            ends_len = 2;
            depth = 0;


            //Work efficient shortest path calculation
            while ( true ) {

                for ( unsigned tid = 0; tid < Q_curr_len; tid++ ) {
                    unsigned v = Q_curr[tid];
                    
                    for ( unsigned j=g.R[v]; j<g.R[v+1]; j++ ) { //for each neighbor of v
                        unsigned w = g.C[j];

                        //intel atomic compare and swap
                        if ( __sync_bool_compare_and_swap ( &d[w], UINT_MAX, ( d[v]+1 ) ) ) {
                            unsigned int temp = __sync_fetch_and_add ( &Q_next_len, 1 );
                            Q_next[temp] = w;

                        }
                        if ( d[w] == ( d[v]+1 ) ) {
                            __sync_fetch_and_add ( & ( sigma[w] ), sigma[v] );
                        }
                    }
                }

                if ( Q_next_len == 0 ) {
                    depth = d[S[S_len-1]];
                    break;
                } else {
                    for ( unsigned tid = 0; tid < Q_next_len; tid++ ) {
                        Q_curr[tid] = Q_next[tid];
                        S[tid + S_len] = Q_next[tid];
                    }
                    ends[ends_len] = ends[ends_len-1] + Q_next_len;
                    ends_len++;
                    Q_curr_len = Q_next_len;
                    S_len = S_len + Q_next_len;
                    Q_next_len = 0;
                }
            }

            //Dependency accumulation
            // skip last iteration (delta values are 0!)
            depth--;
            while ( depth > 0 ) {
                //std::cout << "depth " << depth << " size " << ends.size() << std::endl;

                for ( unsigned tid = ends[depth]; tid < ends[depth+1]; tid++ ) {
                    unsigned w = S[tid];
                    float dsw = 0.0f;
                    unsigned sw = sigma.at ( w );

                    for ( unsigned j=g.R[w]; j<g.R[w+1]; j++ ) { //for each neighbor of v
                        unsigned v = g.C[j];
                        if ( d[v] == ( d[w]+1 ) ) {
                            dsw += ( sw/ ( float ) sigma[v] ) * ( 1+delta[v] );
                        }
                    }

                    delta[w] = dsw;
                }

                depth--;

            }
            for ( unsigned i = 0; i < g.num_vertex; i++ ) {
                if(i != source) {
                    #pragma omp atomic
                    bc[i] += delta[i];
                }
            }

            #ifdef PRINT_UNBALANCE_INFO
                gettimeofday ( &iter_end, NULL );
                double elapsed = ( ( double ) ( ( iter_end.tv_sec * 1000000 + iter_end.tv_usec ) - ( iter_start.tv_sec * 1000000 + iter_start.tv_usec ) ) ) / 1000000;
                iter_time[source] = elapsed;
                source_time[source] = elapsed;
                thread_time[omp_get_thread_num()] += elapsed;
            #endif
        } 
    }

    for ( unsigned i=0; i < g.num_vertex; i++ ) {
        //Undirected edges are modeled as two directed edges, but the scores shouldn't be double counted.
        bc[i] /= 2.0f;
    }

    gettimeofday ( &end, NULL );

    if(printBC) {
        g.print_BC_scores ( bc, output_filename );    
    }

    #ifdef PRINT_UNBALANCE_INFO
        // get min and max
        double min = iter_time[0];
        double max = iter_time[0];

        for ( unsigned source = 1; source < iterations; source++ ) {
            if(iter_time[source] < min) {
                min = iter_time[source];
            }
            if(iter_time[source] > max) {
                max = iter_time[source];
            }
        }

        for ( int thread = 0; thread < omp_get_max_threads(); thread++ ) {
            std::cout << "thread " << thread << ": " << thread_time[thread] << std::endl;
        }

        std::cout << "fastest iteration: " << min << std::endl;
        std::cout << "slowest iteration: " << max << std::endl;
        std::cout << "iterations: " << iterations << std::endl;

        std::cout << "time per source:"  << std::endl;
        for (unsigned it = 0; it < iterations; it++) {
            std::cout << source_time[it] << std::endl;
        }

    #endif


    double elapsed = ( ( double ) ( ( end.tv_sec * 1000000 + end.tv_usec ) - ( start.tv_sec * 1000000 + start.tv_usec ) ) ) /1000000;
    std::cerr << "time " << elapsed << std::endl;

    return 0;
}
