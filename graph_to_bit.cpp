#include <stdio.h>
#include <stdlib.h>
#include "Graph.hpp"

#include <sys/time.h>

int main ( int argc, char *argv[] )
{
    if ( argc < 3 ) {
        std::cout << "Command usage: " << argv[0] << " ./GRAPH_PATH_IN ./GRAPH_PATH_OUT" << std::endl;
        return 0;
    }
    
    Graph g;
    if( g.parse_from_file(argv[1]) != 0 ) {
        std::cerr << "Error opening graph file." << std::endl;
        exit ( -1 );
    }
    if( g.write_to_bitfile(argv[2]) != 0 ) {
        std::cerr << "Error writing bit file." << std::endl;
        exit ( -1 );
    }

    return 0;
}
