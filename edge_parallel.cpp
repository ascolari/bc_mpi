#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <fstream>
#include <algorithm>
#include <iostream>
#include "Graph.hpp"
#include <vector>
#include <queue>
#include <stack>
#include <set>
#include <unistd.h>
#include <cerrno>

#include <sys/time.h>

int main ( int argc, char *argv[] )
{
    struct timeval start, end;
    unsigned source_limit = UINT_MAX;
    bool printBC = false;
    char* output_filename = NULL;

    if ( argc < 2 ) {
        std::cout << "Command usage: " << argv[0] << " [options] ./GRAPH_PATH" << std::endl;
        return 0;
    }

    int c;
    extern char* optarg;
    while ((c = getopt (argc, argv, "s:po:")) != -1) {
        switch (c) {
        case 's':
            errno = 0;
            source_limit = (unsigned) strtoul(optarg, NULL, 10);
            if (errno) {
                std::cout << "max source is in wrong format" << std::endl;
                exit(-1);
            }
            break;
        case 'p':
            printBC = true;
            break;
        case 'o':
            printBC = true;
            output_filename = optarg;
            break;
        }
    }

    // Data structure declararion
    Graph g;
    if( g.parse_from_file ( argv[argc-1] ) != 0 ) {
        std::cerr << "Error opening graph file." << std::endl;
        exit ( -1 );
    }

    std::vector<float> bc ( g.num_vertex,0 );

    std::vector<unsigned> S ( g.num_vertex,0 );
    unsigned S_len = 0;
    std::vector<unsigned> ends ( g.num_vertex+1,0 );
    unsigned ends_len = 0;


    std::vector<unsigned> d ( g.num_vertex, UINT_MAX );
    std::vector<unsigned> sigma ( g.num_vertex,0 );
    std::vector<float> delta ( g.num_vertex,0 );
    unsigned long current_depth = 0;
    unsigned long depth = 0;
    bool done = false;

    gettimeofday ( &start, NULL );

    for ( unsigned source = 0; source < g.num_vertex && source < source_limit; source++ ) {

        //Initialization
        #pragma omp parallel for
        for ( unsigned v = 0; v < g.num_vertex; v++ ) {
            if ( v == source ) {
                d[v] = 0;
                sigma[v] = 1;
            } else {
                d[v] = UINT_MAX;
                sigma[v] = 0;
            }
            delta[v] = 0;
        }
        S[0] = source;
        S_len = 1;
        ends[0] = 1;
        ends[1] = 1;
        ends_len = 2;
        current_depth = 0;
        done = false;

        while ( !done ) {
            done = true;
            #pragma omp parallel for shared(done) shared(S) shared(d) shared(sigma)
            for ( unsigned long edge = 0; edge < g.R[g.num_vertex]; edge+=1 ) {
                unsigned long v = g.F[edge];
                if ( d[v] == current_depth ) {
                    unsigned long w = g.C[edge];
                    if ( __sync_bool_compare_and_swap ( &d[w], UINT_MAX, ( d[v]+1 ) ) )
                        //#pragma omp critical
                        //{
                        //if(d[w] == UINT_MAX)
                    {
                        done = false;
                        d[w] = d[v] + 1;
                        unsigned int temp = __sync_fetch_and_add ( &S_len, 1 );
                        //unsigned int temp = S_len;
                        S[temp] = w;
                        //S_len++;
                    }
                    //}
                    if ( d[w] == ( d[v] + 1 ) ) {
                        #pragma omp atomic
                        sigma[w] += sigma[v];
                    }
                }
            }
            current_depth++;
            ends[ends_len] = S_len;
            ends_len++;
        }

        depth = current_depth;

        //Dependency accumulation

        while ( depth > 0 ) {
            #pragma omp parallel for shared(d) shared(delta)
            for ( unsigned tid = ends[depth]; tid < ends[depth+1]; tid++ ) {
                unsigned w = S[tid];
                float dsw = 0.0f;
                unsigned sw = sigma.at ( w );
                for ( unsigned j=g.R[w]; j<g.R[w+1]; j++ ) { //for each neighbor of v
                    unsigned v = g.C[j];
                    if ( d[v] == ( d[w]+1 ) ) {
                        dsw += ( sw/ ( float ) sigma[v] ) * ( 1+delta[v] );
                    }
                }
                delta[w] = dsw;
            }
            depth--;
        }

        for ( unsigned i = 0; i < g.num_vertex; i++ ) {
            bc[i] += i == source ? 0 : delta[i];
        }
    }
    for ( unsigned i=0; i<g.num_vertex; i++ ) {
        //Undirected edges are modeled as two directed edges, but the scores shouldn't be double counted
        bc[i] /= 2.0f;
    }

    gettimeofday ( &end, NULL );

    if(printBC) {
        g.print_BC_scores ( bc, output_filename );    
    }

    double elapsed = ( ( double ) ( ( end.tv_sec * 1000000 + end.tv_usec ) - ( start.tv_sec * 1000000 + start.tv_usec ) ) ) /1000000;
    std::cerr << "time " << elapsed << std::endl;

    return 0;
}
