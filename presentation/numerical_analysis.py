import os

with open('output_files') as f:
	lines = f.readlines()


minVals = []
maxVals = []

for filename in lines:
	filename = filename.strip()
	if len(filename) > 0:
		with open(filename) as f:
			dataPoints = f.readlines

		if(len(minVals) == 0):
			minVals = [-1.0]*len(dataPoints)
			maxVals = [0.0]*len(dataPoints)

		for it, datum in enumaerate(dataPoints):
			datum = (float)datum
			if(minVals[it] < 0 or minVals[it] > datum):
				minVals[it] = datum
			if(maxVals[it] < datum):
				maxVals[it] = datum


maxAbsError = 0
maxRelError = 0

for it in range(len(minVals)):
	err = maxVals[it] - minVals[it]
	maxAbsError = max(err, maxAbsError)
	relErr = err / minVals[it]
	maxRelError = max(relErr, maxRelError)

print "maxAbsError:" + str(maxAbsError)
print "maxRelError:" + str(maxRelError)



