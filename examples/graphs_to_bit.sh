#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Graphs to binary format conversion..."

mkdir -p "${DIR}/bit"

for path in ${DIR}/*.{mtx,edges,txt}; do
	filename=$(basename ${path})
	graphname="${filename%.*}"
	echo ${graphname}
	../build/g2b ${path} ${DIR}/bit/${graphname}.bit
done

echo "Completed."