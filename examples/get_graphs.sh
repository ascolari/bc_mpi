#!/bin/bash

# graphs from http://networkrepository.com/networks.php

urls=(
'http://nrvis.com/download/data/bio/bio-yeast-protein-inter.zip'
'http://nrvis.com/download/data/dimacs10/delaunay_n14.zip'
'http://nrvis.com/download/data/dimacs10/delaunay_n16.zip'
'http://nrvis.com/download/data/dynamic/fb-wosn-friends.zip'
'http://nrvis.com/download/data/soc/soc-firm-hi-tech.zip'
'http://nrvis.com/download/data/dimacs10/inf-luxembourg_osm.zip'
'http://nrvis.com/download/data/bio/bio-human-gene2.zip'
)

for url in "${urls[@]}"
do
    zipname=$(basename ${url})
    graphname="${zipname%.*}"
    if [[ ! -f ${graphname}.mtx && ! -f ${graphname}.edges && ! -f ${graphname}.txt ]]; then
        wget ${url}
        unzip -o ${zipname}
        if [[ "$1" != 'leave' ]]; then
            rm ${zipname}
        fi
    fi
done

echo "Present graphs"
du -sh ./* | grep "edges\|mtx\|txt" | sort -h

