#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <fstream>
#include <algorithm>
#include <iostream>
#include "Graph.hpp"
#include <vector>
#include <queue>
#include <stack>
#include <set>
#include <atomic>

#include <sys/time.h>

int main ( int argc, char *argv[] )
{
    struct timeval start, end;
    unsigned source_limit = UINT_MAX;
    bool printBC = false;
    char* output_filename = NULL;
    

    if ( argc < 2 ) {
        std::cout << "Command usage: " << argv[0] << " [options] ./GRAPH_PATH" << std::endl;
        return 0;
    }
    
    int c;
    extern char* optarg;
    while ((c = getopt (argc, argv, "s:po:")) != -1) {
        switch (c) {
            case 's':
                errno = 0;
                source_limit = (unsigned) strtoul(optarg, NULL, 10);
                if (errno) {
                    std::cout << "max source is in wrong format" << std::endl;
                    exit(-1);
                }
                break;
            case 'p':
                printBC = true;
                break;
            case 'o':
                printBC = true;
                output_filename = optarg;
                break;
        }
    }   

    Graph g;
    if( g.parse_from_file ( argv[argc-1] ) != 0 ) {
        std::cerr << "Error opening graph file." << std::endl;
        exit ( -1 );
    }

    std::vector<float> bc ( g.num_vertex, 0 );
    
    unsigned *Q_curr = new unsigned[g.num_vertex];
    unsigned Q_curr_len = 0;
    unsigned *Q_next = new unsigned[g.num_vertex];
    unsigned Q_next_len = 0;
    unsigned *tempPtr;

    std::vector<unsigned> S ( g.num_vertex, 0 );
    unsigned S_len = 0;
    std::vector<unsigned> ends ( g.num_vertex + 1, 0 );
    unsigned ends_len = 0;

    #ifdef PRINT_UNBALANCE_INFO
        unsigned ub_max_threads = omp_get_max_threads();
        unsigned *ub_threads_comp_edges = new unsigned[ub_max_threads];
        unsigned ub_lost_work = 0;
        unsigned ub_total_work = 0;
        unsigned ub_max = 0;
    #endif

    int depth = 0;

    std::vector<unsigned> d ( g.num_vertex, UINT_MAX );
    std::vector<unsigned> sigma ( g.num_vertex,0 );
    std::vector<float> delta ( g.num_vertex,0 );

    //take initial time
    gettimeofday ( &start, NULL );
    for ( unsigned source = 0; source < g.num_vertex && source < source_limit; source++ ) {

        //Initialization
        #pragma omp parallel for
        for ( unsigned v = 0; v < g.num_vertex; v++ ) {
            if ( v == source ) {
                d[v] = 0;
                sigma[v] = 1;
            } else {
                d[v] = UINT_MAX;
                sigma[v] = 0;
            }
            delta[v] = 0;
        }
        Q_curr[0] = source;
        Q_curr_len = 1;
        Q_next_len = 0;
        S[0] = source;
        S_len = 1;
        ends[0] = 1;
        ends[1] = 1;
        ends_len = 2;
        depth = 0;


        //Work efficient shortest path calculation
        while ( true ) {

            #ifdef PRINT_UNBALANCE_INFO
                for (unsigned i = 0; i < ub_max_threads; i++) {
                    ub_threads_comp_edges[i] = 0;
                }
            #endif

            #pragma omp parallel for shared(Q_next) shared(Q_curr) shared(d) shared(sigma)
            for ( unsigned tid = 0; tid < Q_curr_len; tid++ ) {
                unsigned v = Q_curr[tid];
                
                #ifdef PRINT_UNBALANCE_INFO
                    ub_threads_comp_edges[omp_get_thread_num()] += (g.R[v+1] - g.R[v]);
                #endif

                for ( unsigned j=g.R[v]; j<g.R[v+1]; j++ ) { //for each neighbor of v
                    unsigned w = g.C[j];

                    //intel atomic compare and swap
                    if ( __sync_bool_compare_and_swap ( &d[w], UINT_MAX, ( d[v]+1 ) ) ) {
                        unsigned int temp = __sync_fetch_and_add ( &Q_next_len, 1 );

                        Q_next[temp] = S[temp + S_len] = w;
                    }
                    if ( d[w] == ( d[v]+1 ) ) {
                        __sync_fetch_and_add ( & ( sigma[w] ), sigma[v] );
                    }
                }
            }

            #ifdef PRINT_UNBALANCE_INFO
                ub_max = 0;
                for (unsigned i = 0; i < ub_max_threads; i++) {
                    ub_total_work += ub_threads_comp_edges[i];
                    ub_max = ub_threads_comp_edges[i] > ub_max ? ub_threads_comp_edges[i] : ub_max; 
                }

                for (unsigned i = 0; i < ub_max_threads; i++) {
                    ub_lost_work += (ub_max - ub_threads_comp_edges[i]);
                }
            #endif

            if ( Q_next_len == 0 ) {
                break;
            }
            
            ends[ends_len] = ends[ends_len-1] + Q_next_len;
            ends_len++;
            Q_curr_len = Q_next_len;
            S_len = S_len + Q_next_len;

            tempPtr = Q_curr;
            Q_curr = Q_next;
            Q_next = tempPtr;

            Q_next_len = 0;
        } 

        //Dependency accumulation
        // skip last iteration (delta values are 0!)
        depth = d[S[S_len-1]] - 1;
        while ( depth > 0 ) {
            //std::cout << "depth " << depth << " size " << ends.size() << std::endl;

            #ifdef PRINT_UNBALANCE_INFO
                for (unsigned i = 0; i < ub_max_threads; i++) {
                    ub_threads_comp_edges[i] = 0;
                }
            #endif

            #pragma omp parallel for shared(d) shared(delta)
            for ( unsigned tid = ends[depth]; tid < ends[depth+1]; tid++ ) {
                unsigned w = S[tid];
                float dsw = 0.0f;
                unsigned sw = sigma.at ( w );

                #ifdef PRINT_UNBALANCE_INFO 
                    ub_threads_comp_edges[omp_get_thread_num()] += (g.R[w+1] - g.R[w]);
                #endif

                for ( unsigned j=g.R[w]; j<g.R[w+1]; j++ ) { //for each neighbor of v
                    unsigned v = g.C[j];
                    if ( d[v] == ( d[w]+1 ) ) {
                        dsw += ( sw/ ( float ) sigma[v] ) * ( 1+delta[v] );
                    }
                }

                delta[w] = dsw;
            }

            depth--;

            #ifdef PRINT_UNBALANCE_INFO
                ub_max = 0;
                for (unsigned i = 0; i < ub_max_threads; i++) {
                    ub_total_work += ub_threads_comp_edges[i];
                    ub_max = ub_threads_comp_edges[i] > ub_max ? ub_threads_comp_edges[i] : ub_max; 
                }

                for (unsigned i = 0; i < ub_max_threads; i++) {
                    ub_lost_work += (ub_max - ub_threads_comp_edges[i]);
                }
            #endif
        }
        for ( unsigned i = 0; i < g.num_vertex; i++ ) {
            //std::cout << "sigma[" << i << "] = " << sigma[i] << std::endl;
            bc[i] += i == source ? 0 : delta[i];
        }
    }

    for ( unsigned i=0; i < g.num_vertex; i++ ) {
        //Undirected edges are modeled as two directed edges, but the scores shouldn't be double counted.
        bc[i] /= 2.0f;
    }

    gettimeofday ( &end, NULL );

    if(printBC) {
        g.print_BC_scores ( bc, output_filename );    
    }

    #ifdef PRINT_UNBALANCE_INFO
        std::cout << "total work: " << (ub_total_work + ub_lost_work) << " active work: " 
                << ub_total_work << " lost work: " << ub_lost_work << " (" 
                << (float)ub_lost_work*100 / (float)(ub_total_work + ub_lost_work) << "%)" << std::endl;
    #endif

    double elapsed = ( ( double ) ( ( end.tv_sec * 1000000 + end.tv_usec ) - ( start.tv_sec * 1000000 + start.tv_usec ) ) ) /1000000;
    std::cerr << "time " << elapsed << std::endl;

    delete Q_curr;
    delete Q_next;

    return 0;
}
