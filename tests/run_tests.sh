#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

programs=(
'../build/we'
'../build/ep'
'../build/sp'
'../build/mpi_sp'
'../build/mpi_sp_dyn'
)

for program in "${programs[@]}"
do
	if [[ -f "${program}" ]]; then
		echo "running test for: ${program}"
		for threads in $(seq 1 4); do

			if [[ "${threads}" == "1" && "${program}" == *"mpi_sp_dyn"* ]]; then
				continue
			fi

			if [[ "${program}" == *"mpi"* ]]; then
				COMMAND="mpiexec -np ${threads} ${program} -o out.txt -t ${threads} test.edges"
				echo "${COMMAND}"
			else
				export OMP_NUM_THREADS=$threads
				COMMAND="${program} -o out.txt test.edges"
				echo "OMP_NUM_THREADS=${threads} ${COMMAND}"
			fi

			$COMMAND

			if [[ -f 'out.txt' ]]; then
				if cmp expected.txt out.txt >/dev/null 2>&1
				then
					echo -e "${GREEN}TEST PASSED!${NC}"  
				else
					echo -e "${RED}TEST FAILED: output not as expected${NC}"
					diff expected.txt out.txt
				fi
				rm out.txt
			else
				echo -e "${RED}TEST FAILED: output not found${NC}"
			fi
			echo ""
			
			
			
		done

		
	else
		echo "WARNING: Unable to find program: ${program}"
	fi
done
